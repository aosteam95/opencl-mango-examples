This repository contains two examples that use the OpenCL wrapper for libmango.

Directory bin
-----------------
This directory contains the binary files related to the kernel functions of the samples.

Directory adapted
-----------------
This directory contains the two OpenCL examples that have been adapted to work over MANGO.

Directory original
------------------
This directory contains the original code of the two OpenCL applications.


Compilation
----------------------
To compile the examples you must have compiled libmango (with the OpenCL wrapper) and you must have installed MANGO

$ mkdir build 

$ cd build

$ cmake .. -DMANGO_PATH="/path/to/mango/" -DOPENCL_WRAPPER_PATH="/path_to_wrapper/" && make

e.g. $ cmake .. -DMANGO_PATH="/opt/mango/" -DOPENCL_WRAPPER_PATH="~/Documents/libmango/" && make


Execution
-----------------
To run the examples you must be using BOSP shell, then you should go to /path_to_sample_dir/build/adapted/<selected_sample>/ and:

$ ./<name_selected_sample>


Also, make sure that you have the binary file for the kernel function, binaries files must be in /path/to/mango/usr/local/share/matrix_multiplication/ directory. 
You can copy the binary inside the bin directoy of this repository and paste them on /path/to/mango/usr/local/share/matrix_multiplication/

Example from the BOSP SHELL:

$ cd /path_to_sample_dir/build/adapted/vector_addition

$ ./Sample_vector_add
